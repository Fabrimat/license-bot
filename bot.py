#!/usr/bin/env python
# -*- encoding: UTF-8 -*-

import os
import sys
sys.path.append('/home/container/packages')

import csv
import discord
from discord.ext import commands
from discord.ext.commands import CommandNotFound
import sqlite3
from sqlite3 import Error
from dotenv import load_dotenv

load_dotenv()

conn = sqlite3.connect('licenses.db')
bot = commands.Bot(command_prefix=os.getenv('COMMAND_PREFIX'))

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

def init_db(conn):
    cur = conn.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS licenses (
        id INTEGER PRIMARY KEY,
        username TEXT NOT NULL UNIQUE,
        password TEXT NOT NULL,
        discord_id TEXT UNIQUE
    )''')
    conn.commit()

def get_total_licenses(conn):
    cur = conn.cursor()
    cur.execute('SELECT COUNT(*) FROM licenses')
    rows = cur.fetchall()

    if(len(rows) > 0):
        return rows[0][0]
    return 0

def get_free_licenses(conn):
    cur = conn.cursor()
    cur.execute('SELECT COUNT(*) FROM licenses WHERE discord_id IS NULL')
    rows = cur.fetchall()

    if(len(rows) > 0):
        return rows[0][0]
    return 0

def get_claimed_licenses(conn):
    cur = conn.cursor()
    cur.execute('SELECT COUNT(*) FROM licenses WHERE discord_id IS NOT NULL')
    rows = cur.fetchall()

    if(len(rows) > 0):
        return rows[0][0]
    return 0

def get_license(conn):
    cur = conn.cursor()
    cur.execute('SELECT * FROM licenses WHERE discord_id IS NULL LIMIT 1')
    rows = cur.fetchall()

    if(len(rows) > 0):
        return (rows[0][0], rows[0][1], rows[0][2])
    return None

def get_user_license(conn, discord_id):
    cur = conn.cursor()
    cur.execute('SELECT * FROM licenses WHERE discord_id = ?', (str(discord_id),))
    rows = cur.fetchall()

    if(len(rows) > 0):
        return (rows[0][0], rows[0][1], rows[0][2])
    return None

def mark_license(conn, license_id, discord_id):
    cur = conn.cursor()
    cur.execute('''UPDATE licenses
        SET discord_id = ?
        WHERE id = ?''', (str(discord_id), str(license_id)))
    conn.commit()

def register_license(conn, discord_id):
    user_license = get_user_license(conn, discord_id)

    if(user_license == None):
        license = get_license(conn)
        if(license != None):
            mark_license(conn, license[0], str(discord_id))
            return (0, license[1], license[2])
        else:
            return None
    else:
        return (1, user_license[1], user_license[2])

def truncate_licenses(conn):
    cur = conn.cursor()
    cur.execute('DELETE FROM licenses')
    conn.commit()

def load_licenses(conn):
    line_count = 0
    license_loaded = 0
    with open('licenses.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        cur = conn.cursor()
        for row in csv_reader:
            if line_count != 0:
                cur.execute('INSERT INTO licenses (username, password) VALUES (?,?)', (row[0], row[1]))
                license_loaded += 1
            line_count += 1
        conn.commit()
        return max(0, license_loaded)
    return license_loaded

@bot.event
async def on_ready():
    print(f'Bot started: {bot.user}')

@bot.command(name = 'licenza')
async def license(ctx, *arg):
    if(len(arg) > 0):
        if(ctx.message.author.guild_permissions.administrator):
            if(arg[0] == 'reload'):
                try:
                    truncate_licenses(conn)
                    loaded = load_licenses(conn)
                    await ctx.send('Caricate {} licenze'.format(loaded))
                except:
                    e = sys.exc_info()[0]
                    await ctx.send(f'Errore durante il caricamento del csv: {e}')
            elif(arg[0] == 'info'):
                total = get_total_licenses(conn)
                claimed = get_claimed_licenses(conn)
                free = get_free_licenses(conn)
                await ctx.send('Ci sono {} licenze.\nRichieste: {}\nLibere: {}'.format(total, claimed, free))
            else:
                await ctx.send('Sottocomando sconosciuto')
        else:
            await ctx.send('Non hai il permesso!')
    else:
        license = register_license(conn, ctx.author.id)
        if(license != None):
            try:
                if(license[0] == 0):
                    await ctx.message.author.send('Ecco qui la licenza che hai richiesto:')
                elif(license[0] == 1):
                    await ctx.message.author.send('Hai già richiesto una licenza:')
                await ctx.message.author.send('Username: `{}`\nPassword: `{}`'.format(license[1], license[2]))
                await ctx.send('Ti ho inviato la licenza tramite messaggio privato')
            except discord.errors.Forbidden:
                await ctx.send('Non sono riuscito a scriverti tramite i messaggi privati. Assicurati di non avermi bloccato e di aver attivato i messaggi privati dai membri di questa gilda!')
        else:
            await ctx.send('Non ci sono licenze disponibili al momento')

@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, CommandNotFound):
        return
    raise error

def main():
    init_db(conn)
    bot.run(os.getenv('DISCORD_TOKEN'))

if __name__ == '__main__':
    main()
